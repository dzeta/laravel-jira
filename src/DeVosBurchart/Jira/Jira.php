<?php namespace DeVosBurchart\Jira;

class Jira
{

	function issues() {
		return new Issue;
	}

	function users() {

	}

	function projects() {

	}

	function caller($method) {
		return Caller::call($method);
	}

	function raw($jql) {
		return new Expression($jql);
	}

}