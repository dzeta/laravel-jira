<?php namespace DeVosBurchart\Jira\Query;

use DeVosBurchart\Jira\Grammar as BaseGrammar;

class Grammar extends BaseGrammar {

	/**
	 * The components that make up a select clause.
	 *
	 * @var array
	 */
	protected $selectComponents = array(
		'wheres',
		'orders'
	);

	/**
	 * Compile a select query into SQL.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder
	 * @return string
	 */
	public function compileSelect(Builder $query)
	{
		return trim($this->concatenate($this->compileComponents($query)));
	}

	/**
	 * Compile the components necessary for a select clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder
	 * @return array
	 */
	protected function compileComponents(Builder $query)
	{
		$sql = array();

		foreach ($this->selectComponents as $component)
		{
			// To compile the query, we'll spin through each component of the query and
			// see if that component exists. If it does we'll just call the compiler
			// function for the component which is responsible for making the SQL.
			if ( ! is_null($query->$component))
			{
				$method = 'compile'.ucfirst($component);

				$sql[$component] = $this->$method($query, $query->$component);
			}
		}

		return $sql;
	}

	/**
	 * Compile the "where" portions of the query.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @return string
	 */
	protected function compileWheres(Builder $query)
	{
		$sql = array();

		if (is_null($query->wheres)) return '';

		// Each type of where clauses has its own compiler function which is responsible
		// for actually creating the where clauses SQL. This helps keep the code nice
		// and maintainable since each clause has a very small method that it uses.
		foreach ($query->wheres as $where)
		{
			$method = "where{$where['type']}";

			$sql[] = $where['boolean'].' '.$this->$method($query, $where);
		}

		// If we actually have some where clauses, we will strip off the first boolean
		// operator, which is added by the query builders for convenience so we can
		// avoid checking for the first clauses in each of the compilers methods.
		if (count($sql) > 0)
		{
			$sql = implode(' ', $sql);

			return preg_replace('/and |or /', '', $sql, 1);
		}

		return '';
	}

	/**
	 * Compile a nested where clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereNested(Builder $query, $where)
	{
		$nested = $where['query'];

		return '('.$this->compileWheres($nested).')';
	}

	/**
	 * Compile a basic where clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereBasic(Builder $query, $where)
	{
		$value = $this->parameter($where['value']);

		return $where['column'].' '.$where['operator'].' '.$value;
	}

	/**
	 * Compile a "where in" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereIn(Builder $query, $where)
	{
		$values = $this->parameterize($where['values']);

		return $where['column'].' in ('.$values.')';
	}

	/**
	 * Compile a "where not in" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereNotIn(Builder $query, $where)
	{
		$values = $this->parameterize($where['values']);

		return $where['column'].' not in ('.$values.')';
	}

	/**
	 * Compile a "where was" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereWas(Builder $query, $where)
	{
		$value = $this->parameter($where['value']);

		return $where['column'].' was '.$value;
	}

	/**
	 * Compile a "where was" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereWasNot(Builder $query, $where)
	{
		$value = $this->parameter($where['value']);

		return $where['column'].' was not '.$value;
	}

	/**
	 * Compile a "where in" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereWasIn(Builder $query, $where)
	{
		$values = $this->parameterize($where['values']);

		return $where['column'].' was in ('.$values.')';
	}

	/**
	 * Compile a "where not in" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereWasNotIn(Builder $query, $where)
	{
		$values = $this->parameterize($where['values']);

		return $where['column'].' was not in ('.$values.')';
	}

	/**
	 * Compile a "where null" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereNull(Builder $query, $where)
	{
		return $where['column'].' is null';
	}

	/**
	 * Compile a "where not null" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereNotNull(Builder $query, $where)
	{
		return $where['column'].' is not null';
	}

	/**
	 * Compile a "where null" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereEmpty(Builder $query, $where)
	{
		return $where['column'].' is empty';
	}

	/**
	 * Compile a "where not null" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereNotEmpty(Builder $query, $where)
	{
		return $where['column'].' is not empty';
	}

	/**
	 * Compile a "where null" clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereChanged(Builder $query, $where)
	{
		return $where['column'].' changed';
	}

	/**
	 * Compile a raw where clause.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $where
	 * @return string
	 */
	protected function whereRaw(Builder $query, $where)
	{
		return $where['jql'];
	}

	/**
	 * Compile the "order by" portions of the query.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @param  array  $orders
	 * @return string
	 */
	protected function compileOrders(Builder $query, $orders)
	{
		return 'order by '.implode(', ', array_map(function($order)
		{
			if (isset($order['sql'])) return $order['sql'];

			return $order['column'].' '.$order['direction'];
		}
		, $orders));
	}

	/**
	 * Concatenate an array of segments, removing empties.
	 *
	 * @param  array   $segments
	 * @return string
	 */
	protected function concatenate($segments)
	{
		return implode(' ', array_filter($segments, function($value)
		{
			return (string) $value !== '';
		}));
	}

	/**
	 * Remove the leading boolean from a statement.
	 *
	 * @param  string  $value
	 * @return string
	 */
	protected function removeLeadingBoolean($value)
	{
		return preg_replace('/and |or /', '', $value, 1);
	}

}
