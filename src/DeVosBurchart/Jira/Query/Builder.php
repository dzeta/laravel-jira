<?php namespace DeVosBurchart\Jira\Query;

use Closure;
use InvalidArgumentException;
use BadMethodCallException;
use DeVosBurchart\Jira\Collection;
use DeVosBurchart\Jira\Caller;
use DeVosBurchart\Jira\Model;
use Cache;

class Builder {

	/**
	 * The database query grammar instance.
	 *
	 * @var \DeVosBurchart\Jira\Query\Grammar
	 */
	protected $grammar;

	/**
	 * The current query value bindings.
	 *
	 * @var array
	 */
	protected $bindings = array();

	/**
	 * The columns that should be returned.
	 *
	 * @var array
	 */
	public $columns;

	/**
	 * The where constraints for the query.
	 *
	 * @var array
	 */
	public $wheres;

	/**
	 * The orderings for the query.
	 *
	 * @var array
	 */
	public $orders;

	/**
	 * The maximum number of records to return.
	 *
	 * @var int
	 */
	public $limit = 50;

	/**
	 * The backups of fields while doing a pagination count.
	 *
	 * @var array
	 */
	protected $backups = array();

	/**
	 * The key that should be used when caching the query.
	 *
	 * @var string
	 */
	protected $cacheKey;

	/**
	 * The number of minutes to cache the query.
	 *
	 * @var int
	 */
	protected $cacheMinutes;

	/**
	 * The tags for the query cache.
	 *
	 * @var array
	 */
	protected $cacheTags;

	/**
	 * The cache driver to be used.
	 *
	 * @var string
	 */
	protected $cacheDriver;

	/**
	 * All of the available clause operators.
	 *
	 * @var array
	 */
	protected $operators = array(
		'=', '<', '>', '<=', '>=', '!='
	);

	protected $cache;


	/**
	 * Create a new query builder instance.
	 *
	 * @param  \DeVosBurchart\Jira\Grammar  $grammar
	 * @return void
	 */
	public function __construct()
	{
		$this->cache = \App::make('cache');
		$this->grammar = new Grammar;
	}

	/**
	 * Add a basic where clause to the query.
	 *
	 * @param  string  $column
	 * @param  string  $operator
	 * @param  mixed   $value
	 * @param  string  $boolean
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 *
	 * @throws \InvalidArgumentException
	 */
	public function where($column, $operator = null, $value = null, $boolean = 'and')
	{
		if (func_num_args() == 2)
		{
			list($value, $operator) = array($operator, '=');
		}
		elseif ($this->invalidOperatorAndValue($operator, $value))
		{
			throw new InvalidArgumentException("Value must be provided.");
		}

		// If the columns is actually a Closure instance, we will assume the developer
		// wants to begin a nested where statement which is wrapped in parenthesis.
		// We'll add that Closure to the query then return back out immediately.
		if ($column instanceof Closure)
		{
			return $this->whereNested($column, $boolean);
		}

		// If the given operator is not found in the list of valid operators we will
		// assume that the developer is just short-cutting the '=' operators and
		// we will set the operators to '=' and set the values appropriately.
		if ( ! in_array(strtolower($operator), $this->operators, true))
		{
			list($value, $operator) = array($operator, '=');
		}

		// If the value is a Closure, it means the developer is performing an entire
		// sub-select within the query and we will need to compile the sub-select
		// within the where clause to get the appropriate query record results.
		if ($value instanceof Closure)
		{
			return $this->whereSub($column, $operator, $value, $boolean);
		}

		// If the value is "null", we will just assume the developer wants to add a
		// where null clause to the query. So, we will allow a short-cut here to
		// that method for convenience so the developer doesn't have to check.
		if (is_null($value))
		{
			return $this->whereNull($column, $boolean, $operator != '=');
		}

		// Now that we are working with just a simple query we can put the elements
		// in our array and add the query binding to our array of bindings that
		// will be bound to each SQL statements when it is finally executed.
		$type = 'Basic';

		$this->wheres[] = compact('type', 'column', 'operator', 'value', 'boolean');

		if ( ! $value instanceof Expression)
		{
			$this->bindings[] = $value;
		}

		return $this;
	}

	/**
	 * Add an "or where" clause to the query.
	 *
	 * @param  string  $column
	 * @param  string  $operator
	 * @param  mixed   $value
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function orWhere($column, $operator = null, $value = null)
	{
		return $this->where($column, $operator, $value, 'or');
	}

	/**
	 * Determine if the given operator and value combination is legal.
	 *
	 * @param  string  $operator
	 * @param  mixed  $value
	 * @return bool
	 */
	protected function invalidOperatorAndValue($operator, $value)
	{
		$isOperator = in_array($operator, $this->operators);

		return ($isOperator && $operator != '=' && is_null($value));
	}

	/**
	 * Add a raw where clause to the query.
	 *
	 * @param  string  $sql
	 * @param  array   $bindings
	 * @param  string  $boolean
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function raw($jql, array $bindings = array(), $boolean = 'and')
	{
		$type = 'raw';

		$this->wheres[] = compact('type', 'jql', 'boolean');

		$this->bindings = array_merge($this->bindings, $bindings);

		return $this;
	}

	/**
	 * Add a raw or where clause to the query.
	 *
	 * @param  string  $sql
	 * @param  array   $bindings
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function orRaw($jql, array $bindings = array())
	{
		return $this->whereRaw($jql, $bindings, 'or');
	}

	/**
	 * Add a nested statement to the query.
	 *
	 * @param  \Closure $callback
	 * @param  string   $boolean
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	public function nest(Closure $callback, $boolean = 'and')
	{
		// To handle nested queries we'll actually create a brand new query instance
		// and pass it off to the Closure that we have. The Closure can simply do
		// do whatever it wants to a query then we will store it for compiling.
		$query = $this->newQuery();

		call_user_func($callback, $query);

		if (count($query->wheres)) {
			$type = 'Nested';

			$this->wheres[] = compact('type', 'query', 'boolean');

			$this->mergeBindings($query);
		}

		return $this;
	}

	/**
	 * Add an or nested statement to the query.
	 *
	 * @param  \Closure $callback
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	public function orNest(Closure $callback)
	{
		return $this->nest($callback, 'or');
	}

	/**
	 * Add a was statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $value
	 * @param  string   $boolean
	 * @param  boolean  $not
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function was($column, $value, $boolean = 'and', $not = false)
	{
		$type = $not ? 'WasNot' : 'Was';

		$this->wheres[] = compact('type', 'column', 'value', 'boolean');

		$this->bindings[] = $value;

		return $this;
	}

	/**
	 * Add an or was statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $value
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orWas($column, $value)
	{
		return $this->was($column, $value, 'or');
	}

	/**
	 * Add a was not statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $value
	 * @param  string   $boolean
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function wasNot($column, $value, $boolean = 'and')
	{
		return $this->was($column, $value, $boolean, true);
	}

	/**
	 * Add an or was not statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $value
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orWasNot($column, $value)
	{
		return $this->wasNot($column, $value, 'or');
	}

	/**
	 * Add an in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @param  string   $boolean
	 * @param  boolean  $not
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function in($column, array $values, $boolean = 'and', $not = false)
	{
		$type = $not ? 'NotIn' : 'In';

		$this->wheres[] = compact('type', 'column', 'values', 'boolean');

		$this->bindings[] = array_merge($this->bindings, $values);

		return $this;
	}

	/**
	 * Add an or in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orIn($column, array $value)
	{
		return $this->in($column, $value, 'or');
	}

	/**
	 * Add a not in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @param  string   $boolean
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function notIn($column, array $value, $boolean = 'and')
	{
		return $this->in($column, $value, $boolean, true);
	}

	/**
	 * Add an or not in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orNotIn($column, array $value)
	{
		return $this->notIn($column, $value, 'or');
	}

	/**
	 * Add a was in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @param  string   $boolean
	 * @param  boolean  $not
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function wasIn($column, array $values, $boolean = 'and', $not = false)
	{
		$type = $not ? 'WasNotIn' : 'WasIn';

		$this->wheres[] = compact('type', 'column', 'values', 'boolean');

		$this->bindings[] = array_merge($this->bindings, $values);

		return $this;
	}

	/**
	 * Add an or was in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orWasIn($column, array $value)
	{
		return $this->wasIn($column, $value, 'or');
	}

	/**
	 * Add a was not in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @param  string   $boolean
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function wasNotIn($column, array $value, $boolean = 'and')
	{
		return $this->wasIn($column, $value, $boolean, true);
	}

	/**
	 * Add an or was not in statement to the query.
	 *
	 * @param  string   $column
	 * @param  array    $values
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orWasNotIn($column, array $value)
	{
		return $this->wasNotIn($column, $value, 'or');
	}

	/**
	 * Add an is empty statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $boolean
	 * @param  boolean  $not
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function isEmpty($column, $boolean = 'and', $not = false)
	{
		$type = $not ? 'NotEmpty' : 'Empty';

		$this->wheres[] = compact('type', 'column', 'boolean');

		return $this;
	}

	/**
	 * Add an or is empty statement to the query.
	 *
	 * @param  string   $column
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orIsEmpty($column)
	{
		return $this->isEmpty($column, 'or');
	}

	/**
	 * Add an is not empty statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $boolean
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function isNotEmpty($column, $boolean = 'and')
	{
		return $this->isEmpty($column, $boolean, true);
	}

	/**
	 * Add an or is not empty statement to the query.
	 *
	 * @param  string   $column
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orIsNotEmpty($column)
	{
		return $this->isNotEmpty($column, 'or');
	}

	/**
	 * Add an is null statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $boolean
	 * @param  boolean  $not
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function isNull($column, $boolean = 'and', $not = false)
	{
		$type = $not ? 'NotNull' : 'Null';

		$this->wheres[] = compact('type', 'column', 'boolean');

		return $this;
	}

	/**
	 * Add an or is null statement to the query.
	 *
	 * @param  string   $column
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orIsNull($column)
	{
		return $this->isNull($column, 'or');
	}

	/**
	 * Add an is not null statement to the query.
	 *
	 * @param  string   $column
	 * @param  string   $boolean
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function isNotNull($column, $boolean = 'and')
	{
		return $this->isNull($column, $boolean, true);
	}

	/**
	 * Add an or is not null statement to the query.
	 *
	 * @param  string   $column
	 * @return \DeVosBurchart\Jira\Builder|static
	 */
	function orIsNotNull($column)
	{
		return $this->isNotNull($column, 'or');
	}

	function changed($column, $filters = null, $boolean = 'and')
	{
		$supportedColumns = array('assignee', 'fixversion', 'priority', 'reporter', 'resolution', 'status');
		$predicates = array('after', 'before', 'by', 'during', 'on', 'from', 'to');

		if(!in_array(strtolower($column), $supportedColumns)) {
			throw new \InvalidArgumentException("Column not supported by 'changed' operator.");
		}

		$type = 'Changed';

		$this->wheres[] = compact('type', 'column', 'boolean');

		return $this;
	}

	function orChanged($column, $filters = null)
	{
		return $this->changed($column, $filters, 'or');
	}

	/**
	 * Add an "order by" clause to the query.
	 *
	 * @param  string  $column
	 * @param  string  $direction
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function orderBy($column, $direction = 'asc')
	{
		$direction = strtolower($direction) == 'asc' ? 'asc' : 'desc';

		$this->orders[] = compact('column', 'direction');

		return $this;
	}

	/**
	 * Add a raw "order by" clause to the query.
	 *
	 * @param  string  $sql
	 * @param  array  $bindings
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function orderByRaw($sql, $bindings = array())
	{
		$type = 'raw';

		$this->orders[] = compact('type', 'sql');

		$this->bindings = array_merge($this->bindings, $bindings);

		return $this;
	}

	/**
	 * Set the "limit" value of the query.
	 *
	 * @param  int  $value
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function limit($value)
	{
		if ($value > 0) $this->limit = $value;

		return $this;
	}

	/**
	 * Alias to set the "limit" value of the query.
	 *
	 * @param  int  $value
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function take($value)
	{
		return $this->limit($value);
	}

	/**
	 * Get the JQL representation of the query.
	 *
	 * @return string
	 */
	public function toJql()
	{
		return $this->grammar->compileSelect($this);
	}

	/**
	 * Alias to getting the JQL representation
	 *
	 * @return string
	 */
	public function toSql()
	{
		return $this->toJql();
	}

	/**
	 * Indicate that the query results should be cached.
	 *
	 * @param  \DateTime|int  $minutes
	 * @param  string  $key
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function remember($minutes, $key = null)
	{
		list($this->cacheMinutes, $this->cacheKey) = array($minutes, $key);

		return $this;
	}

	/**
	 * Indicate that the query results should be cached forever.
	 *
	 * @param  string  $key
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function rememberForever($key = null)
	{
		return $this->remember(-1, $key);
	}

	/**
	 * Indicate that the results, if cached, should use the given cache tags.
	 *
	 * @param  array|dynamic  $cacheTags
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function cacheTags($cacheTags)
	{
		$this->cacheTags = $cacheTags;

		return $this;
	}

	/**
	 * Indicate that the results, if cached, should use the given cache driver.
	 *
	 * @param  string  $cacheDriver
	 * @return \DeVosBurchart\Jira\Query\Builder|static
	 */
	public function cacheDriver($cacheDriver)
	{
		$this->cacheDriver = $cacheDriver;

		return $this;
	}

	/**
	 * Execute a query for a single record by ID.
	 *
	 * @param  int    $id
	 * @param  array  $columns
	 * @return mixed|static
	 */
	public function find($id, $columns = array('*'))
	{
		return $this->where('key', '=', $id)->first($columns);
	}

	/**
	 * Pluck a single column's value from the first result of a query.
	 *
	 * @param  string  $column
	 * @return mixed
	 */
	public function pluck($column)
	{
		$result = (array) $this->first(array($column));

		return count($result) > 0 ? reset($result) : null;
	}

	/**
	 * Execute the query and get the first result.
	 *
	 * @param  array   $columns
	 * @return mixed|static
	 */
	public function first($columns = array('*'))
	{
		$results = $this->take(1)->get($columns);

		return count($results) > 0 ? reset($results) : null;
	}

	/**
	 * Execute the query as a "select" statement.
	 *
	 * @param  array  $columns
	 * @return array|static[]
	 */
	public function get($columns = array('*'))
	{
		if ( ! is_null($this->cacheMinutes)) return $this->getCached($columns);

		return $this->getFresh($columns);
	}

	/**
	 * Execute the query as a fresh "select" statement.
	 *
	 * @param  array  $columns
	 * @return array|static[]
	 */
	public function getFresh($columns = array('*'))
	{
		// if (is_null($this->columns)) $this->columns = $columns;
		$array = $this->runSelect();

		foreach ($array->issues as $v)
		{
			foreach($v->fields as $field => $value) {
				if(is_object($value) && class_exists($class = "DeVosBurchart\Jira\Information\\" . studly_case($field))) {
					$model = new $class;
					$v->{$field} = $model->newFromBuilder($value);
				} else {
					$v->{$field} = $value;
				}
			}
			$v->priority_id = $v->priority->id;
			unset($v->fields);
		}
		
		return $array->issues;
	}

	/**
	 * Run the query as a "select" statement against the connection.
	 *
	 * @return array
	 */
	protected function runSelect()
	{
		return Caller::call('search?jql=' . urlencode($this->toSql()) . '&maxResults=' . $this->limit);
	}

	/**
	 * Execute the query as a cached "select" statement.
	 *
	 * @param  array  $columns
	 * @return array
	 */
	public function getCached($columns = array('*'))
	{
		if (is_null($this->columns)) $this->columns = $columns;

		// If the query is requested to be cached, we will cache it using a unique key
		// for this database connection and query statement, including the bindings
		// that are used on this query, providing great convenience when caching.
		list($key, $minutes) = $this->getCacheInfo();

		$cache = $this->getCache();

		$callback = $this->getCacheCallback($columns);

		// If the "minutes" value is less than zero, we will use that as the indicator
		// that the value should be remembered values should be stored indefinitely
		// and if we have minutes we will use the typical remember function here.
		if ($minutes < 0)
		{
			return $cache->rememberForever($key, $callback);
		}
		else
		{
			return $cache->remember($key, $minutes, $callback);
		}
	}

	/**
	 * Get the cache object with tags assigned, if applicable.
	 *
	 * @return \Illuminate\Cache\CacheManager
	 */
	protected function getCache()
	{
		$cache = $this->getCacheManager()->driver($this->cacheDriver);

		return $this->cacheTags ? $cache->tags($this->cacheTags) : $cache;
	}

	/**
	 * Get the cache manager instance.
	 *
	 * @return \Illuminate\Cache\CacheManager
	 */
	public function getCacheManager()
	{
		if ($this->cache instanceof Closure)
		{
			$this->cache = call_user_func($this->cache);
		}

		return $this->cache;
	}

	/**
	 * Set the cache manager instance on the connection.
	 *
	 * @param  \Illuminate\Cache\CacheManager|\Closure  $cache
	 * @return void
	 */
	public function setCacheManager($cache)
	{
		$this->cache = $cache;
	}

	/**
	 * Get the cache key and cache minutes as an array.
	 *
	 * @return array
	 */
	protected function getCacheInfo()
	{
		return array($this->getCacheKey(), $this->cacheMinutes);
	}

	/**
	 * Get a unique cache key for the complete query.
	 *
	 * @return string
	 */
	public function getCacheKey()
	{
		return $this->cacheKey ?: $this->generateCacheKey();
	}

	/**
	 * Generate the unique cache key for the query.
	 *
	 * @return string
	 */
	public function generateCacheKey()
	{
		$name = 'Jira';

		return md5($name.$this->toSql().serialize($this->bindings));
	}

	/**
	 * Get the Closure callback used when caching queries.
	 *
	 * @param  array  $columns
	 * @return \Closure
	 */
	protected function getCacheCallback($columns)
	{
		$me = $this;

		return function() use ($me, $columns) { return $me->getFresh($columns); };
	}

	/**
	 * Chunk the results of the query.
	 *
	 * @param  int  $count
	 * @param  callable  $callback
	 * @return void
	 */
	public function chunk($count, $callback)
	{
		$results = $this->forPage($page = 1, $count)->get();

		while (count($results) > 0)
		{
			// On each chunk result set, we will pass them to the callback and then let the
			// developer take care of everything within the callback, which allows us to
			// keep the memory low for spinning through large result sets for working.
			call_user_func($callback, $results);

			$page++;

			$results = $this->forPage($page, $count)->get();
		}
	}

	/**
	 * Get an array with the values of a given column.
	 *
	 * @param  string  $column
	 * @param  string  $key
	 * @return array
	 */
	public function lists($column, $key = null)
	{
		$columns = $this->getListSelect($column, $key);

		// First we will just get all of the column values for the record result set
		// then we can associate those values with the column if it was specified
		// otherwise we can just give these values back without a specific key.
		$results = new Collection($this->get($columns));

		$values = $results->fetch($columns[0])->all();

		// If a key was specified and we have results, we will go ahead and combine
		// the values with the keys of all of the records so that the values can
		// be accessed by the key of the rows instead of simply being numeric.
		if ( ! is_null($key) && count($results) > 0)
		{
			$keys = $results->fetch($key)->all();

			return array_combine($keys, $values);
		}

		return $values;
	}

	/**
	 * Get the columns that should be used in a list array.
	 *
	 * @param  string  $column
	 * @param  string  $key
	 * @return array
	 */
	protected function getListSelect($column, $key)
	{
		$select = is_null($key) ? array($column) : array($column, $key);

		// If the selected column contains a "dot", we will remove it so that the list
		// operation can run normally. Specifying the table is not needed, since we
		// really want the names of the columns as it is in this resulting array.
		if (($dot = strpos($select[0], '.')) !== false)
		{
			$select[0] = substr($select[0], $dot + 1);
		}

		return $select;
	}

	/**
	 * Concatenate values of a given column as a string.
	 *
	 * @param  string  $column
	 * @param  string  $glue
	 * @return string
	 */
	public function implode($column, $glue = null)
	{
		if (is_null($glue)) return implode($this->lists($column));

		return implode($glue, $this->lists($column));
	}

	/**
	 * Get a paginator for the "select" statement.
	 *
	 * @param  int    $perPage
	 * @param  array  $columns
	 * @return \Illuminate\Pagination\Paginator
	 */
	public function paginate($perPage = 15, $columns = array('*'))
	{
		$paginator = $this->connection->getPaginator();

		if (isset($this->groups))
		{
			return $this->groupedPaginate($paginator, $perPage, $columns);
		}
		else
		{
			return $this->ungroupedPaginate($paginator, $perPage, $columns);
		}
	}

	/**
	 * Create a paginator for a grouped pagination statement.
	 *
	 * @param  \Illuminate\Pagination\Environment  $paginator
	 * @param  int    $perPage
	 * @param  array  $columns
	 * @return \Illuminate\Pagination\Paginator
	 */
	protected function groupedPaginate($paginator, $perPage, $columns)
	{
		$results = $this->get($columns);

		return $this->buildRawPaginator($paginator, $results, $perPage);
	}

	/**
	 * Build a paginator instance from a raw result array.
	 *
	 * @param  \Illuminate\Pagination\Environment  $paginator
	 * @param  array  $results
	 * @param  int    $perPage
	 * @return \Illuminate\Pagination\Paginator
	 */
	public function buildRawPaginator($paginator, $results, $perPage)
	{
		// For queries which have a group by, we will actually retrieve the entire set
		// of rows from the table and "slice" them via PHP. This is inefficient and
		// the developer must be aware of this behavior; however, it's an option.
		$start = ($paginator->getCurrentPage() - 1) * $perPage;

		$sliced = array_slice($results, $start, $perPage);

		return $paginator->make($sliced, count($results), $perPage);
	}

	/**
	 * Create a paginator for an un-grouped pagination statement.
	 *
	 * @param  \Illuminate\Pagination\Environment  $paginator
	 * @param  int    $perPage
	 * @param  array  $columns
	 * @return \Illuminate\Pagination\Paginator
	 */
	protected function ungroupedPaginate($paginator, $perPage, $columns)
	{
		$total = $this->getPaginationCount();

		// Once we have the total number of records to be paginated, we can grab the
		// current page and the result array. Then we are ready to create a brand
		// new Paginator instances for the results which will create the links.
		$page = $paginator->getCurrentPage($total);

		$results = $this->forPage($page, $perPage)->get($columns);

		return $paginator->make($results, $total, $perPage);
	}

	/**
	 * Get the count of the total records for pagination.
	 *
	 * @return int
	 */
	public function getPaginationCount()
	{
		$this->backupFieldsForCount();

		$columns = $this->columns;

		// Because some database engines may throw errors if we leave the ordering
		// statements on the query, we will "back them up" and remove them from
		// the query. Once we have the count we will put them back onto this.
		$total = $this->count();

		$this->restoreFieldsForCount();

		// Once the query is run we need to put the old select columns back on the
		// instance so that the select query will run properly. Otherwise, they
		// will be cleared, then the query will fire with all of the columns.
		$this->columns = $columns;

		return $total;
	}

	/**
	 * Backup certain fields for a pagination count.
	 *
	 * @return void
	 */
	protected function backupFieldsForCount()
	{
		foreach (array('orders', 'limit', 'offset') as $field)
		{
			$this->backups[$field] = $this->{$field};

			$this->{$field} = null;
		}

	}

	/**
	 * Restore certain fields for a pagination count.
	 *
	 * @return void
	 */
	protected function restoreFieldsForCount()
	{
		foreach (array('orders', 'limit', 'offset') as $field)
		{
			$this->{$field} = $this->backups[$field];
		}

		$this->backups = array();
	}

	/**
	 * Insert a new record into the database.
	 *
	 * @param  array  $values
	 * @return bool
	 */
	public function insert(array $values)
	{
		// Since every insert gets treated like a batch insert, we will make sure the
		// bindings are structured in a way that is convenient for building these
		// inserts statements by verifying the elements are actually an array.
		if ( ! is_array(reset($values)))
		{
			$values = array($values);
		}

		// Since every insert gets treated like a batch insert, we will make sure the
		// bindings are structured in a way that is convenient for building these
		// inserts statements by verifying the elements are actually an array.
		else
		{
			foreach ($values as $key => $value)
			{
				ksort($value); $values[$key] = $value;
			}
		}
		
		$values = array_map(function($record) {
			$record['project'] = array('key' => $record['project']);
		
			if(isset($record['parent']))
				$record['parent'] = array('key' => $record['parent']);

			return $record;
		}, $values);

		$response = array();
		foreach ($values as $record)
		{
			$response[] = Caller::post('issue', ['fields' => $record]);
		}

		return $response;
	}

	/**
	 * Insert a new record and get the value of the primary key.
	 *
	 * @param  array   $values
	 * @param  string  $sequence
	 * @return int
	 */
	public function insertGetId(array $values, $sequence = null)
	{
		$sql = $this->grammar->compileInsertGetId($this, $values, $sequence);

		$values = $this->cleanBindings($values);

		return $this->processor->processInsertGetId($this, $sql, $values, $sequence);
	}

	/**
	 * Update a record in the database.
	 *
	 * @param  array  $values
	 * @return int
	 */
	public function update(array $values)
	{
		$bindings = array_values(array_merge($values, $this->bindings));

		$sql = $this->grammar->compileUpdate($this, $values);

		return $this->connection->update($sql, $this->cleanBindings($bindings));
	}

	/**
	 * Delete a record from the database.
	 *
	 * @param  mixed  $id
	 * @return int
	 */
	public function delete($id = null)
	{
		// If an ID is passed to the method, we will set the where clause to check
		// the ID to allow developers to simply and quickly remove a single row
		// from their database without manually specifying the where clauses.
		if ( ! is_null($id)) $this->where('id', '=', $id);

		$sql = $this->grammar->compileDelete($this);

		return $this->connection->delete($sql, $this->bindings);
	}

	/**
	 * Get a new instance of the query builder.
	 *
	 * @return \DeVosBurchart\Jira\Query\Builder
	 */
	public function newQuery()
	{
		return new Builder($this->connection, $this->grammar, $this->processor);
	}

	/**
	 * Merge an array of where clauses and bindings.
	 *
	 * @param  array  $wheres
	 * @param  array  $bindings
	 * @return void
	 */
	public function mergeWheres($wheres, $bindings)
	{
		$this->wheres = array_merge((array) $this->wheres, (array) $wheres);

		$this->bindings = array_values(array_merge($this->bindings, (array) $bindings));
	}

	/**
	 * Remove all of the expressions from a list of bindings.
	 *
	 * @param  array  $bindings
	 * @return array
	 */
	protected function cleanBindings(array $bindings)
	{
		return array_values(array_filter($bindings, function($binding)
		{
			return ! $binding instanceof Expression;
		}));
	}

	/**
	 * Get the current query value bindings.
	 *
	 * @return array
	 */
	public function getBindings()
	{
		return $this->bindings;
	}

	/**
	 * Set the bindings on the query builder.
	 *
	 * @param  array  $bindings
	 * @return \DeVosBurchart\Jira\Query\Builder
	 */
	public function setBindings(array $bindings)
	{
		$this->bindings = $bindings;

		return $this;
	}

	/**
	 * Add a binding to the query.
	 *
	 * @param  mixed  $value
	 * @return \DeVosBurchart\Jira\Query\Builder
	 */
	public function addBinding($value)
	{
		$this->bindings[] = $value;

		return $this;
	}

	/**
	 * Merge an array of bindings into our bindings.
	 *
	 * @param  \DeVosBurchart\Jira\Query\Builder  $query
	 * @return \DeVosBurchart\Jira\Query\Builder
	 */
	public function mergeBindings(Builder $query)
	{
		$this->bindings = array_values(array_merge($this->bindings, $query->bindings));

		return $this;
	}

	/**
	 * Get the query grammar instance.
	 *
	 * @return \DeVosBurchart\Jira\Grammar
	 */
	public function getGrammar()
	{
		return $this->grammar;
	}

	/**
	 * Handle dynamic method calls into the method.
	 *
	 * @param  string  $method
	 * @param  array   $parameters
	 * @return mixed
	 *
	 * @throws \BadMethodCallException
	 */
	public function __call($method, $parameters)
	{
		if (starts_with($method, 'where'))
		{
			return $this->dynamicWhere($method, $parameters);
		}

		$className = get_class($this);

		throw new BadMethodCallException("Call to undefined method {$className}::{$method}()");
	}

}
