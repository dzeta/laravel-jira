<?php namespace DeVosBurchart\Jira;

use InvalidArgumentException, Config;

class Caller {
	
	static function curl($method, $username, $password, array $config = array()) {
		$ch = curl_init();
		
		curl_setopt_array($ch, self::curlConfig($method, $username, $password, $config));
		
		$result = curl_exec($ch);
		
		curl_close($ch);

		return json_decode($result);
	}

	static function curlConfig($method, $username, $password, array $custom = array()) {
		return $custom + array(
			CURLOPT_URL => Config::get('jira::url') . '/rest/api/latest/' . $method,
			CURLOPT_USERPWD => $username . ':' . $password,
			CURLOPT_HTTPHEADER => array('Content-type: application/json'),
			CURLOPT_RETURNTRANSFER => true
		);
	}

	static function get($method, $username, $password)
	{
		return self::curl($method, $username, $password);
	}

	static function put($method, array $data, $username = null, $password = null) {
		if(is_null($username) && is_null($password)) {
			$username = Config::get('jira::username');
			$password = Config::get('jira::password');
		}
		
		return self::curl($method, $username, $password, array(
			CURLOPT_CUSTOMREQUEST => 'PUT',
			CURLOPT_POSTFIELDS => json_encode($data),
			CURLOPT_HTTPHEADER => array(
				'Accept: application/json',
				'Content-Type: application/json'
			)
		));
	}

	static function post($method, array $data, $username = null, $password = null) {
		if(is_null($username) && is_null($password)) {
			$username = Config::get('jira::username');
			$password = Config::get('jira::password');
		}

		return self::curl($method, $username, $password, array(
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => json_encode($data),
		));
	}

	static function delete() {

	}

	static function secure($method, $username, $password, $data, $type = 'POST')
	{
		switch($type) {
			case 'POST':
				return self::post($method, $data, $username, $password);
			case 'PUT':
				return self::put($method, $data, $username, $password);
			default:
				throw new InvalidArgumentException('Type needs to be POST or PUT');
		}
	}

	static function call($method)
	{
		return self::get($method, Config::get('jira::username'), Config::get('jira::password'));
	}

}