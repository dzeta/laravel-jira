<?php namespace DeVosBurchart\Jira\Information;

use DeVosBurchart\Jira\Model;

class Status extends Model {
	
	function getIconAttribute() {
		return $this->iconUrl;
	}

}