<?php namespace DeVosBurchart\Jira\Information;

use DeVosBurchart\Jira\Model;

class Issuetype extends Model {
	
	function getIconAttribute() {
		return $this->iconUrl;
	}

}