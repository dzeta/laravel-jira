<?php namespace DeVosBurchart\Jira\Information;

use DeVosBurchart\Jira\Model;

class Progress extends Model {
	
	function getPercentAttribute($value) {
		if(empty($value)) return '0';
		return $value;
	}

}